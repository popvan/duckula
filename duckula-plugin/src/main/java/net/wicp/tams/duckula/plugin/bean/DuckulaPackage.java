package net.wicp.tams.duckula.plugin.bean;

import lombok.Data;

@Data
public class DuckulaPackage {
	private EventTable eventTable;
	private int rowsNum;
	private boolean isError;
	private String[][] befores;
	private String[][] afters;
}
