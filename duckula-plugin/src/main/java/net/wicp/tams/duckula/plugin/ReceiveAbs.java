package net.wicp.tams.duckula.plugin;

import com.alibaba.fastjson.JSONObject;

public abstract class ReceiveAbs implements IReceive {
	public static final String colParam = "colParam";
	public static final String colTaskId = "colTaskId";

	public static final String colTopic = "colTopic";
	public static final String colPartitions = "colPartitions";
	public static final String colKey = "colKey";
	public static final String colSplitkey = "colSplitkey";
	public static final String colOther = "colOther";

	protected final JSONObject params;
	protected final String taskId;
	// protected final Logger log;

	public ReceiveAbs(JSONObject paramObjs) {
		params = paramObjs.getJSONObject(colParam);
		taskId = paramObjs.getString(colTaskId);
	}
}
