package net.wicp.tams.duckula.plugin;

import java.util.List;

import net.wicp.tams.duckula.plugin.bean.DuckulaPackage;
import net.wicp.tams.duckula.plugin.bean.SingleRecord;

/***
 * 系列化
 * 
 * @author rjzjh
 *
 */
public interface ISerialize {
	public List<SingleRecord> serialize(DuckulaPackage duckulaPackage,String splitKey);
}
