package net.wicp.tams.duckula.plugin.bean;

import lombok.Builder;
import lombok.Data;
import net.wicp.tams.common.constant.OptType;

@Data
@Builder
public class SingleRecord {
	private String key;
	private String db;
	private String tb;
	private OptType optType;
	private byte[] data;
}
