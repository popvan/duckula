package net.wicp.tams.duckula.plugin.bean;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import net.wicp.tams.duckula.plugin.constant.RuleItem;

@Data
public class Rule {
	private String dbPattern;
	private String tbPattern;
	private String splitKey;
	private String remark;
	Map<RuleItem, String> items = new HashMap<>();
}
