package net.wicp.tams.duckula.plugin;

import java.util.List;

import net.wicp.tams.duckula.plugin.bean.DuckulaPackage;
import net.wicp.tams.duckula.plugin.bean.Rule;
import net.wicp.tams.duckula.plugin.bean.SingleRecord;

/***
 * 构造函数需要一个参数Map<String,String>
 * 
 * @author rjzjh
 *
 */
public interface IReceive {
	/***
	 * 未序列化数据
	 * 
	 * @param duckulaPackage
	 * @return
	 */
	public boolean receiveMsg(DuckulaPackage duckulaPackage,Rule rule);// 插件必须实现

	/***
	 * 序列化后的数据
	 */
	public boolean receiveMsg(List<SingleRecord> data,Rule rule);// 插件必须实现
}
