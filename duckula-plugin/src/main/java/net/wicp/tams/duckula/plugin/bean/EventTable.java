package net.wicp.tams.duckula.plugin.bean;

import lombok.Data;
import net.wicp.tams.common.constant.OptType;

@Data
public class EventTable {
	private String db;
	private String tb;
	private OptType OptType;// 0 insert 1update 2delete
	private String gtid;
	/** 列数，一般与cols长度一致，不一致说明cols与binlog不同步 **/
	private int colsNum;
	private String[] cols;
	private int[] colsType;
}
