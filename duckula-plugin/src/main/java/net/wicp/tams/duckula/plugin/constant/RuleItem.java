package net.wicp.tams.duckula.plugin.constant;

public enum RuleItem {
	topic("消息主题"),

	key("redis的keys"),

	splitkey("分库分表键"),

	other("未知参数");

	private final String desc;

	private RuleItem(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public static RuleItem get(String name) {
		for (RuleItem ruleItem : RuleItem.values()) {
			if (ruleItem.name().equalsIgnoreCase(name)) {
				return ruleItem;
			}
		}
		return null;
	}
}
