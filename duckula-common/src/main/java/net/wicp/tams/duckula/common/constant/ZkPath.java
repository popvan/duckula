package net.wicp.tams.duckula.common.constant;

import net.wicp.tams.duckula.common.beans.ColHis;

public enum ZkPath {
	tasks("任务路径", "/duckula/tasks"),
	
	tasksoffline("离线任务路径", "/duckula/tasksoffline"),

	pos("位置路径", "/duckula/pos"),

	counts("计数器路径", "/duckula/counts"),
	
	dbinsts("数据库实例配置", "/duckula/dbinsts"),
	
	servers("执行服务器配置", "/duckula/servers"),

	cols("列名的路径", "/duckula/cols");

	private final String desc;
	private final String path;

	private ZkPath(String desc, String path) {
		this.desc = desc;
		this.path = path;
	}

	public String getDesc() {
		return desc;
	}

	public String getRoot() {
		return this.path;
	}
	
	public String getPath(String taskId) {
		return String.format("%s/%s", this.path, taskId);
	}

	public String getColsPath(String taskId, ColHis colhis) {
		if (this != ZkPath.cols) {
			throw new IllegalAccessError("	只有列名的路径能访问此方法");
		}
		return String.format("%s/%s/%s|%s", this.path, taskId, colhis.getDb(), colhis.getTb());
	}

	public static ZkPath getByName(String zkPath) {
		for (ZkPath ele : ZkPath.values()) {
			if (ele.name().equalsIgnoreCase(zkPath)) {
				return ele;
			}
		}
		return null;
	}
}
