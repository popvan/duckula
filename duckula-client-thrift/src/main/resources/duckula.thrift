namespace java net.wicp.tams.duckula.client


enum OptType {
  insert = 1,
  deleted = 2,
  update = 3
}

enum ColumnType { 
    DECIMAL    = 0,
    TINY       = 1,
    SHORT      = 2,
    LONG       = 3,
    FLOAT      = 4,
    DOUBLE     = 5,
    NULL       = 6,
    TIMESTAMP  = 7,
    LONGLONG   = 8,
    INT24      = 9,
    DATE       = 10,
    TIME       = 11,
    DATETIME   = 12,
    YEAR       = 13,
    NEWDATE    = 14,
    VARCHAR    = 15,
    BIT        = 16,
    TIMESTAMP2 = 17,
    DATETIME2  = 18,
    TIME2      = 19,
    JSON       = 245,
    NEWDECIMAL = 246,
    ENUM       = 247,
    SET        = 248,
    TINY_BLOB  = 249,
    MEDIUM_BLOB= 250,
    LONG_BLOB  = 251,
    BLOB       = 252,
    VAR_STRING = 253,
    STRING     = 254,
    GEOMETRY   = 255
}

struct DuckulaEvent {
  1: string db
  2: string tb
  3: OptType optType
  4: string gtid
  5: i32  colNum
  6: list<string> cols
  7: list<ColumnType> colsType
  8: map<string,string> before
  9: map<string,string> after
  10: bool  isError  
}