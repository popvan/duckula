/**
 * Autogenerated by Thrift Compiler (0.10.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package net.wicp.tams.duckula.client;


import java.util.Map;
import java.util.HashMap;
import org.apache.thrift.TEnum;

public enum ColumnType implements org.apache.thrift.TEnum {
  DECIMAL(0),
  TINY(1),
  SHORT(2),
  LONG(3),
  FLOAT(4),
  DOUBLE(5),
  NULL(6),
  TIMESTAMP(7),
  LONGLONG(8),
  INT24(9),
  DATE(10),
  TIME(11),
  DATETIME(12),
  YEAR(13),
  NEWDATE(14),
  VARCHAR(15),
  BIT(16),
  TIMESTAMP2(17),
  DATETIME2(18),
  TIME2(19),
  JSON(245),
  NEWDECIMAL(246),
  ENUM(247),
  SET(248),
  TINY_BLOB(249),
  MEDIUM_BLOB(250),
  LONG_BLOB(251),
  BLOB(252),
  VAR_STRING(253),
  STRING(254),
  GEOMETRY(255);

  private final int value;

  private ColumnType(int value) {
    this.value = value;
  }

  /**
   * Get the integer value of this enum value, as defined in the Thrift IDL.
   */
  public int getValue() {
    return value;
  }

  /**
   * Find a the enum type by its integer value, as defined in the Thrift IDL.
   * @return null if the value is not found.
   */
  public static ColumnType findByValue(int value) { 
    switch (value) {
      case 0:
        return DECIMAL;
      case 1:
        return TINY;
      case 2:
        return SHORT;
      case 3:
        return LONG;
      case 4:
        return FLOAT;
      case 5:
        return DOUBLE;
      case 6:
        return NULL;
      case 7:
        return TIMESTAMP;
      case 8:
        return LONGLONG;
      case 9:
        return INT24;
      case 10:
        return DATE;
      case 11:
        return TIME;
      case 12:
        return DATETIME;
      case 13:
        return YEAR;
      case 14:
        return NEWDATE;
      case 15:
        return VARCHAR;
      case 16:
        return BIT;
      case 17:
        return TIMESTAMP2;
      case 18:
        return DATETIME2;
      case 19:
        return TIME2;
      case 245:
        return JSON;
      case 246:
        return NEWDECIMAL;
      case 247:
        return ENUM;
      case 248:
        return SET;
      case 249:
        return TINY_BLOB;
      case 250:
        return MEDIUM_BLOB;
      case 251:
        return LONG_BLOB;
      case 252:
        return BLOB;
      case 253:
        return VAR_STRING;
      case 254:
        return STRING;
      case 255:
        return GEOMETRY;
      default:
        return null;
    }
  }
}
