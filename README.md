# duckula
分布式binlog监听中间件.duckula能像吸血鬼一样从各mysql实例中得到变化的数据,又能自动复活(HA).支持插件化数据接收者和序列化.非常灵活.


# Features
duckula是一套分布式binlog监听中间件,可在自定义接收者,项目内置了kafka和redis接收者,也可以自定义序列化格式,项目也内置了protobuf2/protobuf3序列化.通过插件的形式嵌入到duckula.
具有丰富的界面操作,可以通过页面操作进行任务的创建,任务的启动与停止.合适的调度策略,当某个task由于某种原因"自杀"后,系统会自动选择其它占用资源较少的服务器来运行此task.支持GTID和文件名+位置的2种形式的起动监听方式,且可以选择以前的任何位点进行监听(只要能起得来).支持docker.支持metrics的计数(现只是打好日志,后续会加入界面查看).

#　模块说明
[INFO] duckula .......................总项目规范,一些jar包/maven插件的定义

[INFO] duckula-plugin ................ 自定义接收者(receive)需要引入此模块.

[INFO] duckula-plugin-redis ...........redis接收者,它会把mysql的变化信息直接放redis

[INFO] duckula-client-protobuf3 .......protobuf3的客户端,如果是使用protobuf3序列化的消息时,业务方需要引入此jar包做反序列化

[INFO] duckula-serializer-protobuf3 ... protobuf3的序化化实现

[INFO] duckula-common ................. 公共项目/ops,task等会共用它

[INFO] duckula-task ...................duckula的主程序,binlog的监听靠它实现,使用serializer/receive也是它

[INFO] duckula-ops  ................. ops控制台,duckula的调度,ＨＡ都由它发起.

[INFO] duckula-server ................ 微服务,每个可以跑task的服务器都需要部这个项目,接收ops发过来的启停task等指令并返回结果

[INFO] duckula-client-protobuf2 ...... protobuf2的客户端,如果是使用protobuf2序列化的消息时,业务方需要引入此jar包做反序列化

[INFO] duckula-serializer-protobuf2 ..protobuf2的序化化实现

[INFO] duckula-plugin-kafka .........kafka接收者,它会把mysql的变化信息推送到kafka,对于同一条记录的的变化信息保证它的顺序性.

[INFO] duckula-install ..............项目的部署程序,会自动生成一个大的tar包

[INFO] ------------------------------------------------------------------------

[INFO] BUILD SUCCESS

[INFO] ------------------------------------------------------------------------



#	插件classload的结构:
　　taskClassLoad

         |
         
         |
         
       serializerClassLoad(序列化)
       
       					|
       					
       					|
       					
       				receiveClassLoad(消息接收者)	
       				
#	mysql的配置:
　 duckula只监听row模式的binlog,
[mysqld]

gtid_mode=ON

log-slave-updates=ON

enforce-gtid-consistency=ON


//log-bin=mysql_bin.log

log-bin=/binlog/mysql_bin.log

binlog-format=ROW


#	安装
  1　mvn clean install -Dmaven.test.skip=true
  
  ２　(可选)　在duckula-install/target/duckula执行　docker build -f DockerfileOps -t="duckulaops:1" .
  
  ３　准备３台机,把duckula-install/target/duckuladuckula.tar复制并解压到３台机, 配置环境变量:DUCKULA_HOME, 一台运行　duckula-ops.war,两台运行　duckula-server.jar
  
  4 　启动ops配置好task,选择其中一台运行这个task
  
#	配置和运行
　设置好环境变量:　DUCKULA_HOME　　　,它就是duckula整个项目的根目录,目录结构请做完install命令后的duckula-install/target/duckula文件夹　

　修改目录的duckula/duckula-install/conf　的相关配置信息,文件名与模块名相同

# 步骤:
1. 安装zookeeper:
2. 按实际情况修改地址zk地址:
     duckula-install\conf\duckula-ops.properties
     duckula-install\conf\duckula-task.properties
3. 在duckula项目(顶级项目)下执行: mvn clean install -Dmaven.test.skip=true
4. 设置环境变量: DUCKULA_HOME 目录为: duckula-install\target\duckula
5. 启动ops配置好一个任务,URL: http://localhost:9090/duckula-ops/
    (1) 配置好数据库实例
    (2)配置好任务,如: world`city`{'topic':'t1'} 表示 监听world库city表,发消息到t1这个topic
         更多规则:https://gitee.com/rjzjh/duckula/wikis/%E7%9B%91%E5%90%AC%E8%A7%84%E5%88%99
6. 启动task实例

#	docker 
  1　ops 的docker化,做完install后在duckula-install/target/duckula目录下执行:
  
  　　docker build -f DockerfileOps -t="duckulaops:1" .
  
  2　产生duckula-task images(如果配置走docker,在启动server时会自动创建该image,该imagesId应与duckula-server.properties配置的imageid一致):
 
     docker build -t="duckulatask:1" .
     
  3  要求安装server的机器要安装docker并且http://localhost:2375　这个地址能访问,具体配置请自行google
  
   4  默认不走docker直接创建进程,如果要启用docker配置,修改 duckula-server.properties的duckula.docker.enable属性
   
  5  duckula-server不建议打docker包,它需要直接运行在主机上,它要启停docker容器.

#	配置要求
　JDK7+

 zookeeper 3.5.2
 
 mysql5.6+ (5.6支持gtid,低版本可以跑,但不支持gtid)
 
 其它跟据不同插件选择不同的中间件.
 
 
 

       				
       			
