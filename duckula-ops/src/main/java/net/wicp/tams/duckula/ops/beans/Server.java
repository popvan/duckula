package net.wicp.tams.duckula.ops.beans;

import java.util.List;

import lombok.Data;
import net.wicp.tams.common.beans.MonitorInfo;
import net.wicp.tams.duckula.common.ZkClient;
import net.wicp.tams.duckula.common.constant.ZkPath;

@Data
public class Server implements Comparable<Server> {
	private String ip;
	private String name;
	private String serverPort;// server端提供的端口即微服务的端口
	private String lockIp;//用于查在分布式锁上用的IP。默认与ip相同，也有可能不同
	private String remark;
	private Boolean run;// ops为构造那构运行的树

	private MonitorInfo mi;

	public List<String> findTasks() {
		List<String> tasks = ZkClient.getInst().getChildren(ZkPath.servers.getRoot());
		return tasks;
	}

	@Override
	public int compareTo(Server s) {
		if (this.mi == null || s.mi == null) {
			return 0;
		}
		long def = this.mi.getFreeMemory() - s.getMi().getFreeMemory();
		return def > 0 ? 1 : -1;
	}
}
