package net.wicp.tams.duckula.ops.pages;

import java.util.List;

import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.ioc.internal.util.CollectionFactory;

import net.wicp.tams.component.assistbean.Menu;
import net.wicp.tams.component.constant.ResType;

public class Main {

	@OnEvent(value = "switchMenu")
	public List<Menu> switchMenu(String moudleId) {
		Menu tempnume = Menu.builder().id("a").resName("dddd").resType(ResType.url).resValue("/index").build();
		return CollectionFactory.newList(tempnume);
	}

}
