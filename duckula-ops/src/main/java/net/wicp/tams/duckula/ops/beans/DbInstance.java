package net.wicp.tams.duckula.ops.beans;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.aliyuncs.rds.model.v20140815.DescribeDBInstanceHAConfigResponse.NodeInfo;

import lombok.Data;
import net.wicp.tams.common.constant.dic.YesOrNo;

@Data
public class DbInstance {
	private String id;
	private String url;
	private int port;
	private String user;
	private String pwd;
	private List<NodeInfo> nodes;
	private YesOrNo isWhileList;// 佐罗服务器是否加入白名单

	/**
	 * 主结点的Nodeid
	 * 
	 * @return
	 */
	public String getMaster() {
		if (CollectionUtils.isEmpty(nodes)) {
			return "";
		}
		for (NodeInfo nodeInfo : nodes) {
			if ("Master".equals(nodeInfo.getNodeType())) {
				return nodeInfo.getNodeId();
			}
		}
		return "";
	}
}
