package net.wicp.tams.duckula.ops.servicesBusi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.RollingFileAppender;
import net.wicp.tams.common.apiext.IOUtil;
import net.wicp.tams.common.apiext.StringUtil;
import net.wicp.tams.common.constant.DateFormatCase;
import net.wicp.tams.duckula.common.beans.Pos;

public abstract class DuckulaUtils {
	private static Set<Long> hasLoggers = new HashSet<>();

	/***
	 * 跟据masterId得到对应的logger
	 * 
	 * @param masterId
	 * @return
	 */
	public static Logger getPosLog(long masterId) {
		Logger logger = null;
		if (!hasLoggers.contains(masterId)) {
			String logRoot = System.getenv("DUCKULA_HOME") + "/logs/pos";
			final String filepath = "%s/%s/pos%s.log";
			RollingFileAppender<ILoggingEvent> fileAppender = LogBackUtil.newFileAppender(
					String.format(filepath, logRoot, masterId, ""), "%msg%n",
					String.format(filepath, logRoot, masterId, "-%d{yyyy-MM-dd}"), 30);
			logger = (Logger) LoggerFactory.getLogger(String.valueOf(masterId));// za_sleuth
			logger.addAppender(fileAppender);
			logger.setLevel(Level.INFO);
			hasLoggers.add(masterId);
		} else {
			logger = (Logger) LoggerFactory.getLogger(String.valueOf(masterId));// za_sleuth
		}
		return logger;
	}

	/***
	 * 得到当前的主的的位点
	 * 
	 * @param masterId
	 * @return
	 */
	public static String openPosLog(long masterId, Date queryDate) {
		String logRoot = IOUtil.mergeFolderAndFilePath(System.getenv("DUCKULA_HOME"), "/logs/pos");
		String path = "";
		if (queryDate == null || DateUtils.isSameDay(queryDate, new Date())) {
			path = String.format("%s/%s/pos.log", logRoot, masterId);
		} else {
			path = String.format("%s/%s/pos-%s.log", logRoot, masterId,
					DateFormatCase.YYYY_MM_DD.getInstanc().format(queryDate));
		}
		return path;
	}

	public static List<Pos> readPosLogReverse(long masterId, Date queryDate, int maxNum) {
		Set<Pos> retpos = new HashSet<>();
		String path = openPosLog(masterId, queryDate);
		List<Pos> ret = new ArrayList<>();
		List<String> retlist = IOUtil.readFileReverse(path, maxNum);
		if (CollectionUtils.isEmpty(retlist)) {
			return ret;
		}
		for (String str : retlist) {
			if (StringUtil.isNull(str)) {
				continue;
			}
			retpos.add(JSONObject.parseObject(str, Pos.class));
		}
		ret.addAll(retpos);
		Collections.sort(ret);
		return ret;
	}

	public static List<Pos> readPosLogReverse(long masterId, int maxNum) {
		return readPosLogReverse(masterId, null, maxNum);
	}

	public static JSONObject buildClient(Command command) {
		JSONObject retobj = new JSONObject();
		JSONObject clientobj = new JSONObject();
		clientobj.put("senderSystem", "duckula");
		clientobj.put("senderApplication", "ops");
		clientobj.put("version", "1.0");
		clientobj.put("senderChannel", "H5");
		clientobj.put("msgId", "1493708268936");
		clientobj.put("requestCommand", command.name());
		retobj.put("ControlInfo", clientobj);
		return retobj;
	}

}
