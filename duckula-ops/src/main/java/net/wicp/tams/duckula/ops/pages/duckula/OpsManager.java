package net.wicp.tams.duckula.ops.pages.duckula;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.RequestGlobals;
import org.apache.tapestry5.util.TextStreamResponse;
import org.apache.zookeeper.KeeperException;

import net.wicp.tams.common.Result;
import net.wicp.tams.common.apiext.CollectionUtil;
import net.wicp.tams.common.apiext.StringUtil;
import net.wicp.tams.common.apiext.json.EasyUiAssist;
import net.wicp.tams.common.callback.IConvertValue;
import net.wicp.tams.component.services.IReq;
import net.wicp.tams.component.tools.TapestryAssist;
import net.wicp.tams.duckula.common.ZkUtil;
import net.wicp.tams.duckula.common.beans.Pos;
import net.wicp.tams.duckula.common.beans.Task;
import net.wicp.tams.duckula.common.constant.ZkPath;
import net.wicp.tams.duckula.ops.beans.PosShow;
import net.wicp.tams.duckula.ops.beans.Server;
import net.wicp.tams.duckula.ops.servicesBusi.DuckulaUtils;
import net.wicp.tams.duckula.ops.servicesBusi.IDuckulaAssit;

public class OpsManager {

	@Inject
	protected RequestGlobals requestGlobals;

	@Inject
	protected Request request;

	@Inject
	private IDuckulaAssit duckulaAssit;

	@Inject
	private IReq req;

	@SuppressWarnings("unchecked")
	public TextStreamResponse onQuery() throws KeeperException, InterruptedException {
		final Task taskparam = TapestryAssist.getBeanFromPage(Task.class, requestGlobals);
		List<PosShow> taskPosList = duckulaAssit.findAllPosForTasks();
		List<PosShow> retlist = (List<PosShow>) CollectionUtils.select(taskPosList, new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				PosShow temp = (PosShow) object;
				boolean ret = true;
				if (StringUtil.isNotNull(taskparam.getId())) {
					ret = temp.getId().indexOf(taskparam.getId()) >= 0;
					if (!ret) {
						return false;
					}
				}
				return ret;
			}

		});
		List<Server> findAllServers = duckulaAssit.findAllServers();
		for (PosShow posShow : retlist) {
			List<String> ips = ZkUtil.lockIps(posShow.getId());
			if (CollectionUtils.isEmpty(ips)) {
				posShow.setHostNum(0);
			} else {				
				Task buidlTask = ZkUtil.buidlTask(posShow.getId());
				List<String> lockToServer = duckulaAssit.lockToServer(findAllServers, buidlTask);
				posShow.setLockIPs(CollectionUtil.listJoin(lockToServer, ","));
				posShow.setHostNum(ips.size());
			}
		}

		String retstr = EasyUiAssist.getJsonForGrid(retlist,
				new String[] { "id", "gtids", "masterServerId", "fileName", "pos", "time", "timeStr", "lockIPs",
						"hostNum" },
				new IConvertValue[] { null, null, null, null, null, null, null, null, null }, retlist.size());
		return TapestryAssist.getTextStreamResponse(retstr);
	}

	public TextStreamResponse onQueryPos() throws KeeperException, InterruptedException {
		PosShow posshow = TapestryAssist.getBeanFromPage(PosShow.class, requestGlobals);
		if (StringUtil.isNull(posshow.getId())) {
			return TapestryAssist.getTextStreamResponseEmpty();
		}
		// String path = String.format("%s/%s", zorroAssit.getPathPos(),
		// posshow.getId());
		// String posData = zk.getData(path);
		// Pos pos = JSONObject.parseObject(posData, Pos.class);

		List<Pos> selPos = DuckulaUtils.readPosLogReverse(posshow.getMasterServerId(), 35);
		String retstr = EasyUiAssist.getJsonForGrid(selPos,
				new String[] { "gtids", "masterServerId", "fileName", "pos", "time", "timeStr" }, selPos.size());
		return TapestryAssist.getTextStreamResponse(retstr);
	}

	public TextStreamResponse onSavePos() throws KeeperException, InterruptedException {
		Pos pos = TapestryAssist.getBeanFromPage(Pos.class, requestGlobals);
		String taskId = request.getParameter("id");
		ZkUtil.updatePos(taskId, pos);
		return TapestryAssist.getTextStreamResponse(Result.getSuc());
	}

	public TextStreamResponse onDelPos() {
		PosShow posshow = TapestryAssist.getBeanFromPage(PosShow.class, requestGlobals);
		Result result = ZkUtil.del(ZkPath.pos, posshow.getId());
		return TapestryAssist.getTextStreamResponse(result);
	}

}
