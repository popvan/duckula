package net.wicp.tams.duckula.ops.servicesBusi;

import java.util.List;
import java.util.Map;

import net.wicp.tams.common.Result;
import net.wicp.tams.duckula.common.beans.Task;
import net.wicp.tams.duckula.ops.beans.DbInstance;
import net.wicp.tams.duckula.ops.beans.PosShow;
import net.wicp.tams.duckula.ops.beans.Server;

public interface IDuckulaAssit {
	
	public static final String  urlFormat="http://%s:%s/duckula-server/connector";
	
	
	public List<Server> findAllServers();

	public List<DbInstance> findAllDbInstances();

	public List<Task> findAllTasks();

	public List<PosShow> findAllPosForTasks();

	public Map<String, Integer> serverRunTaskNum(List<Server> servers);
	
	public List<String> lockToServer(List<Server> findAllServers,Task task);//把锁值里面显示的IP转成对应的Server IP

	/***
	 * 找到内存最大的机器
	 */
	public Server selServer(String... removeIps);

	public Result startTask(String taskId, Server server,boolean isAuto);

	public Result stopTask(String taskId, Server server);
}
