package net.wicp.tams.duckula.ops.pages.duckula;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.RequestGlobals;
import org.apache.tapestry5.util.TextStreamResponse;
import org.apache.zookeeper.KeeperException;

import com.alibaba.fastjson.JSONObject;

import lombok.extern.slf4j.Slf4j;
import net.wicp.tams.common.apiext.NumberUtil;
import net.wicp.tams.common.apiext.StringUtil;
import net.wicp.tams.common.apiext.json.EasyUiAssist;
import net.wicp.tams.common.beans.MonitorInfo;
import net.wicp.tams.common.callback.IConvertValue;
import net.wicp.tams.common.http.HttpClient;
import net.wicp.tams.common.http.HttpResult;
import net.wicp.tams.component.services.IReq;
import net.wicp.tams.component.tools.TapestryAssist;
import net.wicp.tams.duckula.ops.beans.Server;
import net.wicp.tams.duckula.ops.servicesBusi.Command;
import net.wicp.tams.duckula.ops.servicesBusi.DuckulaUtils;
import net.wicp.tams.duckula.ops.servicesBusi.IDuckulaAssit;

@Slf4j
public class ViewServer {
	@Inject
	protected RequestGlobals requestGlobals;

	@Inject
	protected Request request;

	@Inject
	private IDuckulaAssit duckulaAssit;
	@Inject
	private IReq req;

	//String queryServerUrlformat = "http://%s:%s/duckula-server/monitorshow";

	@SuppressWarnings("unchecked")
	public TextStreamResponse onQuery() throws KeeperException, InterruptedException {
		final Server serverParam = TapestryAssist.getBeanFromPage(Server.class, requestGlobals);
		List<Server> servers = duckulaAssit.findAllServers();
		List<Server> retlist = (List<Server>) CollectionUtils.select(servers, new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				Server temp = (Server) object;
				boolean ret = true;
				if (StringUtil.isNotNull(serverParam.getIp())) {
					ret = temp.getIp().indexOf(serverParam.getIp()) >= 0;
					if (!ret) {
						return false;
					}
				}
				return ret;
			}
		});

		for (Server server : retlist) {
			try {
				JSONObject params = DuckulaUtils.buildClient(Command.MonitorShow);
				params.put("needCpu", "false");

				HttpResult res = HttpClient
						.doPost(String.format(IDuckulaAssit.urlFormat, server.getIp(), server.getServerPort()), params);
				
				JSONObject retobj=(JSONObject)JSONObject.parse(res.getBodyStr());
				MonitorInfo monitorInfoBean = JSONObject.parseObject(retobj.getJSONObject("monitorInfo").toJSONString(), MonitorInfo.class);
				server.setMi(monitorInfoBean);
			} catch (Exception e) {
				log.error("获取服务器信息失败", e);
			}
		}

		IConvertValue<String> freeConv = new IConvertValue<String>() {
			@Override
			public String getStr(String keyObj) {
				double d = Long.parseLong(keyObj) / (double) 1024;
				BigDecimal temp = NumberUtil.handleScale(d, 2);
				return temp.doubleValue() + "M";
			}
		};

		IConvertValue<String> cpuConv = new IConvertValue<String>() {
			@Override
			public String getStr(String keyObj) {
				if(StringUtil.isNull(keyObj)) {
					return "未统计";
				}else {
					double d = Double.parseDouble(keyObj);
					BigDecimal temp = NumberUtil.handleScale(d, 2);
					return temp.doubleValue() + "%";
				}
				
			}
		};

		String retstr = EasyUiAssist.getJsonForGrid(retlist,
				new String[] { "ip", "name", "serverPort", "remark", "mi.cpuRatio,cpuRatio",
						"mi.freePhysicalMemorySize,fm", "mi.totalMemorySize,am" },
				new IConvertValue[] { null, null, null, null, cpuConv, freeConv, freeConv }, retlist.size());
		return TapestryAssist.getTextStreamResponse(retstr);
	}
}
