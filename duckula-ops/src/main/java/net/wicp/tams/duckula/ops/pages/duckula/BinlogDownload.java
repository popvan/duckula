package net.wicp.tams.duckula.ops.pages.duckula;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.io.FileUtils;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.internal.util.CollectionFactory;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.RequestGlobals;
import org.apache.tapestry5.util.TextStreamResponse;
import org.apache.zookeeper.KeeperException;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.rds.model.v20140815.DescribeBinlogFilesResponse.BinLogFile;
import com.aliyuncs.rds.model.v20140815.DescribeDBInstanceHAConfigResponse.NodeInfo;

import net.wicp.tams.common.Conf;
import net.wicp.tams.common.Result;
import net.wicp.tams.common.apiext.IOUtil;
import net.wicp.tams.common.apiext.StringUtil;
import net.wicp.tams.common.apiext.TarUtil;
import net.wicp.tams.common.apiext.json.EasyUiAssist;
import net.wicp.tams.common.apiext.json.JSONUtil;
import net.wicp.tams.common.callback.IConvertValue;
import net.wicp.tams.common.callback.impl.convertvalue.ConvertValueSize;
import net.wicp.tams.common.constant.DateFormatCase;
import net.wicp.tams.common.constant.dic.SizeUnit;
import net.wicp.tams.common.others.RdsUtil;
import net.wicp.tams.component.services.IReq;
import net.wicp.tams.component.tools.TapestryAssist;
import net.wicp.tams.duckula.common.ZkClient;
import net.wicp.tams.duckula.common.ZkUtil;
import net.wicp.tams.duckula.common.beans.Task;
import net.wicp.tams.duckula.common.beans.TaskOffline;
import net.wicp.tams.duckula.common.constant.ZkPath;
import net.wicp.tams.duckula.ops.beans.DbInstance;

public class BinlogDownload {

	@Inject
	protected RequestGlobals requestGlobals;

	@Inject
	protected Request request;
	@Inject
	private IReq req;

	public TextStreamResponse onQuery() throws KeeperException, InterruptedException, ParseException {
		String dbId = request.getParameter("dbId");
		String hostId = request.getParameter("hostId");
		String beginDate = request.getParameter("beginDate");
		String endDate = request.getParameter("endDate");
		if (StringUtil.isNull(dbId) || StringUtil.isNull(hostId) || StringUtil.isNull(beginDate)) {
			return TapestryAssist.getTextStreamResponse(EasyUiAssist.getJsonForGridEmpty());
		}
		Date startTime = DateFormatCase.YYYY_MM_DD.getInstanc().parse(beginDate);
		Date endTime = StringUtil.isNull(endDate) ? new Date() : DateFormatCase.YYYY_MM_DD.getInstanc().parse(endDate);
		List<BinLogFile> binlogfiles = RdsUtil.findBinLogFilesMax(dbId, hostId, startTime, endTime);

		IConvertValue<String> fileconv = new IConvertValue<String>() {
			@Override
			public String getStr(String keyObj) {
				String filename = StringUtil.getFileName(keyObj);
				return filename;
			}
		};
		Map<String, IConvertValue<String>> conmap = new HashMap<>();
		conmap.put("filename", fileconv);
		conmap.put("fileSize1", new ConvertValueSize(SizeUnit.B, SizeUnit.MB));

		String logDir = IOUtil.mergeFolderAndFilePath(System.getenv("DUCKULA_HOME"), "/logs/binlog", hostId);
		// Collection<File> files = FileUtils.listFiles(new File(logDir), new
		// String[] { "tar" }, true);
		File dir = new File(logDir);
		final List<String> fileNames = new ArrayList<>();
		if (dir.exists()) {
			File[] files = dir.listFiles();
			for (File file : files) {
				fileNames.add(file.getName());
			}
		}

		// 文件是否存在
		IConvertValue<String> fileExit = new IConvertValue<String>() {
			@Override
			public String getStr(String keyObj) {
				String filename_tar = StringUtil.getFileName(keyObj);
				String filename = filename_tar.replace(".tar", "");
				int retint = 0;
				if (fileNames.contains(filename)) {
					retint += 2;
				}
				if (fileNames.contains(filename_tar)) {
					retint += 1;
				}
				return String.valueOf(retint);
			}
		};
		conmap.put("fileexit", fileExit);

		String retstr = EasyUiAssist.getJsonForGridAlias(binlogfiles,
				new String[] { "downloadLink,filename", "downloadLink,fileexit", "fileSize,fileSize1","hostInstanceID,hostId" }, conmap,
				(long) binlogfiles.size());
		return TapestryAssist.getTextStreamResponse(retstr);
	}

	public TextStreamResponse onQueryDbInsts() throws KeeperException, InterruptedException {
		List<String> dbs = ZkClient.getInst().getChildren(ZkPath.dbinsts.getRoot());
		String retstr = JSONUtil.getJsonForListSimple(dbs);
		return TapestryAssist.getTextStreamResponse(retstr);
	}

	public TextStreamResponse onDownloadFile() throws KeeperException, InterruptedException, IOException {
		BinLogFile binLogFile = TapestryAssist.getBeanFromPage(BinLogFile.class, request);
		String logDirPath = IOUtil.mergeFolderAndFilePath(System.getenv("DUCKULA_HOME"), "/logs/binlog",
				binLogFile.getHostInstanceID());
		FileUtils.forceMkdir(new File(logDirPath));
		String str = RdsUtil.downloadBinLogFiles(binLogFile, logDirPath);
		TarUtil.decompress(str);
		return TapestryAssist.getTextStreamResponse(Result.getSuc(str));
	}

	public TextStreamResponse onDelTarAndLog() throws KeeperException, InterruptedException, IOException {
		BinLogFile binLogFile = TapestryAssist.getBeanFromPage(BinLogFile.class, request);
		String filename_tar = request.getParameter("filename");
		String filename = filename_tar.replace(".tar", "");
		String logDir = IOUtil.mergeFolderAndFilePath(Conf.get("zorro.binlog.dir"), binLogFile.getHostInstanceID());
		File file_tar = new File(IOUtil.mergeFolderAndFilePath(logDir, filename_tar));
		if (file_tar.exists()) {
			FileUtils.forceDelete(file_tar);
		}
		File file = new File(IOUtil.mergeFolderAndFilePath(logDir, filename));
		if (file.exists()) {
			FileUtils.forceDelete(file);
		}
		return TapestryAssist.getTextStreamResponse(Result.getSuc());
	}

	@SuppressWarnings("unchecked")
	public TextStreamResponse onQueryHostInsts() throws KeeperException, InterruptedException {

		String jsonStr = "";
		if (!request.getParameterNames().contains("parent")) {
			jsonStr = "[]";
		} else {
			final String parentid = request.getParameter("parent");

			// String path = StringUtil.isNull(parentid) ? zorroAssit.getPathInstance()
			// : String.format("%s/%s", zorroAssit.getPathInstance(), parentid);
			DbInstance temp = JSONObject.toJavaObject(ZkClient.getInst().getZkData(ZkPath.dbinsts.getPath(parentid)),
					DbInstance.class);

			IConvertValue<Object> conver = new IConvertValue<Object>() {
				@Override
				public String getStr(Object keyObj) {
					NodeInfo temp = (NodeInfo) keyObj;
					return String.format("%s:%s", temp.getNodeId(), temp.getNodeType());
				}
			};
			jsonStr = JSONUtil.getJsonForList(temp.getNodes(), new IConvertValue[] { null, conver }, "nodeId,value",
					",text");
		}
		return TapestryAssist.getTextStreamResponse(jsonStr);
	}

	@SuppressWarnings("unchecked")
	public TextStreamResponse onQueryTasks() throws KeeperException, InterruptedException {
		final String queryDbId = request.getParameter("queryDbId");
		List<String> taskNodes = ZkClient.getInst().getChildren(ZkPath.tasks.getRoot());
		List<Task> tasks = CollectionFactory.newList();
		for (String nodeName : taskNodes) {
			Task temp = ZkUtil.buidlTask(nodeName);
			tasks.add(temp);
		}

		List<Task> retlist = (List<Task>) CollectionUtils.select(tasks, new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				Task temp = (Task) object;

				if (queryDbId.equals(temp.getDbinst())) {
					return true;
				} else {
					return false;
				}
			}

		});
		String retstr = EasyUiAssist.getJsonForGrid(retlist, new String[] { "id", "ip", "rules", "senderEnum" },
				new IConvertValue[] { null, null, null, null, null }, retlist.size());
		return TapestryAssist.getTextStreamResponse(retstr);
	}

	public TextStreamResponse onSaveOfflineTask() throws KeeperException, InterruptedException {
		final TaskOffline taskparam = TapestryAssist.getBeanFromPage(TaskOffline.class, requestGlobals);
		if (taskparam.getTimeBegin() !=null) {
			taskparam.setLimitType(1);
		} else if (StringUtil.isNull(taskparam.getGtidBegin()) && StringUtil.isNull(taskparam.getGtidEnd())) {
			taskparam.setLimitType(2);
		} else {
			return req.retErrorInfo("不支持的限定类型，现在只支持时间和gtid两种类型");
		}
		Task buidlTask = ZkUtil.buidlTask(taskparam.getTaskOnlineId());
		if (buidlTask == null) {
			return req.retErrorInfo("需要在线task");
		}
		taskparam.setTaskOnline(buidlTask);
		taskparam.setBinlogFiles(taskparam.getBinlogFiles().replaceAll(".tar", ""));
		// Stat stat = ZkUtil.exists(ZkPath.tasksoffline, taskparam.getId());
		ZkClient.getInst().createOrUpdateNode(ZkPath.tasksoffline.getPath(taskparam.getId()),
				JSONObject.toJSONString(taskparam));
		return req.retSuccInfo("保存Task成功");
	}

}
