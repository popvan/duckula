package net.wicp.tams.duckula.ops.pages.duckula;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.RequestGlobals;
import org.apache.tapestry5.util.TextStreamResponse;
import org.apache.zookeeper.KeeperException;

import com.alibaba.fastjson.JSON;

import lombok.extern.slf4j.Slf4j;
import net.wicp.tams.common.Result;
import net.wicp.tams.common.apiext.StringUtil;
import net.wicp.tams.common.apiext.json.EasyUiAssist;
import net.wicp.tams.common.callback.IConvertValue;
import net.wicp.tams.component.services.IReq;
import net.wicp.tams.component.tools.TapestryAssist;
import net.wicp.tams.duckula.common.ZkClient;
import net.wicp.tams.duckula.common.constant.ZkPath;
import net.wicp.tams.duckula.ops.beans.Server;
import net.wicp.tams.duckula.ops.servicesBusi.IDuckulaAssit;

/***
 * 服务器管理
 * 
 * @author zhoujunhui
 *
 */
@Slf4j
public class ServerManager {
	@Inject
	protected RequestGlobals requestGlobals;

	@Inject
	protected Request request;

	@Inject
	private IReq req;
	@Inject
	private IDuckulaAssit duckulaAssit;

	@SuppressWarnings("unchecked")
	public TextStreamResponse onQuery() throws KeeperException, InterruptedException {
		final Server serverParam = TapestryAssist.getBeanFromPage(Server.class, requestGlobals);
		List<Server> servers = duckulaAssit.findAllServers();
		List<Server> retlist = (List<Server>) CollectionUtils.select(servers, new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				Server temp = (Server) object;
				boolean ret = true;
				if (StringUtil.isNotNull(serverParam.getIp())) {
					ret = temp.getIp().indexOf(serverParam.getIp()) >= 0;
					if (!ret) {
						return false;
					}
				}
				return ret;
			}

		});

		final Map<String, Integer> maps = duckulaAssit.serverRunTaskNum(retlist);
		IConvertValue<String> taskDetail = new IConvertValue<String>() {
			@Override
			public String getStr(String keyObj) {
				return String.valueOf(maps.get(keyObj));
			}

		};

		String retstr = EasyUiAssist.getJsonForGrid(retlist,
				new String[] { "ip", "name", "serverPort","lockIp","remark", "ip,taskNum" },
				new IConvertValue[] { null, null, null, null,null,taskDetail }, retlist.size());
		return TapestryAssist.getTextStreamResponse(retstr);
	}

	public TextStreamResponse onSave() {
		final Server serverParam = TapestryAssist.getBeanFromPage(Server.class, requestGlobals);
		Result result = ZkClient.getInst().createOrUpdateNode(ZkPath.servers.getPath(serverParam.getIp()),
				JSON.toJSONString(serverParam));

		return TapestryAssist.getTextStreamResponse(result);
	}

	public TextStreamResponse onDel() throws KeeperException, InterruptedException {
		final Server serverparam = TapestryAssist.getBeanFromPage(Server.class, requestGlobals);
		Result result = ZkClient.getInst().deleteNode(ZkPath.servers.getPath(serverparam.getIp()));
		return TapestryAssist.getTextStreamResponse(result);
	}
}
