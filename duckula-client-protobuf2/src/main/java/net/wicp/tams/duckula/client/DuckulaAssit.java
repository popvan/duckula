package net.wicp.tams.duckula.client;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.apache.commons.codec.binary.Base64;
import com.google.protobuf.InvalidProtocolBufferException;
import net.wicp.tams.duckula.client.Protobuf2.ColumnType;
import net.wicp.tams.duckula.client.Protobuf2.DuckulaCol;
import net.wicp.tams.duckula.client.Protobuf2.DuckulaEvent;

public abstract class DuckulaAssit {
	public static SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static DuckulaEvent parse(byte[] data) throws InvalidProtocolBufferException {
		DuckulaEvent retobj = DuckulaEvent.parseFrom(data);
		return retobj;
	}

	public static boolean isEmpty(CharSequence cs) {
		return cs == null || cs.length() == 0;
	}

	/***
	 * 得到变化后数据的值
	 * 
	 * @param duckulaEvent
	 * @param colName
	 * @return
	 */
	public static <T extends Serializable> T getValueAfter(DuckulaEvent duckulaEvent, String colName) {
		return getValue(duckulaEvent, colName, true);
	}

	public static <T extends Serializable> T getValueBefore(DuckulaEvent duckulaEvent, String colName) {
		return getValue(duckulaEvent, colName, false);
	}

	@SuppressWarnings("unchecked")
	public static <T extends Serializable> T getValue(DuckulaEvent duckulaEvent, String colName, boolean isAfter) {
		if (duckulaEvent.getColsCount() != duckulaEvent.getColsList().size()) {
			throw new RuntimeException("列名与值不一致，请联系相关人员。");
		}

		int colindex = duckulaEvent.getColsList().indexOf(colName);
		if (colindex < 0) {
			return null;
		}
		DuckulaCol duckulaCol = null;
		if (isAfter) {
			duckulaCol = duckulaEvent.getAfter(colindex);
		} else {
			duckulaCol = duckulaEvent.getBefore(colindex);
		}
		ColumnType columnType = duckulaEvent.getColsType(colindex);

		Serializable retobj = null;
		String value = duckulaCol.getColValue();
		switch (columnType) {
		case LONGLONG:
			retobj = Long.valueOf(value);
			break;
		case BIT:
		case TINY:
		case SHORT:
		case INT24:
		case LONG:
		case ENUM:
		case SET:
			retobj = Integer.valueOf(value);
			break;
		case FLOAT:
			retobj = Float.valueOf(value);
			break;

		case DOUBLE:
			retobj = Double.valueOf(value);
			break;
		case DECIMAL:
		case NEWDECIMAL:
			retobj = new BigDecimal(value);
			break;
		case BLOB:
		case GEOMETRY:
			try {
				retobj = Base64.decodeBase64(value);
			} catch (Exception e) {
				retobj = value;
			}
			break;
		case YEAR:
			try {
				retobj = Integer.valueOf(value);
			} catch (Exception e) {
				retobj = value;
			}
			break;
		case TIMESTAMP2:
		case DATETIME2:
			try {
				retobj = formater.parse(value);
			} catch (ParseException e) {
				retobj = value;
			}
			break;

		default:
			retobj = value;
			break;
		}
		return (T) retobj;
	}

	public static byte[] getBytes(String filePath) {
		byte[] buffer = null;
		try {
			File file = new File(filePath);
			FileInputStream fis = new FileInputStream(file);
			ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
			byte[] b = new byte[1000];
			int n;
			while ((n = fis.read(b)) != -1) {
				bos.write(b, 0, n);
			}
			fis.close();
			bos.close();
			buffer = bos.toByteArray();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return buffer;
	}
}
