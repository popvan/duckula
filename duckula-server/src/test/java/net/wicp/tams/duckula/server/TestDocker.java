package net.wicp.tams.duckula.server;

import java.net.URI;
import java.util.List;

import org.junit.Test;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.DockerClient.ListContainersParam;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;

public class TestDocker {

	@Test
	public void listContainer() throws DockerException, InterruptedException {
		final DockerClient docker = DefaultDockerClient.builder().uri(URI.create("http://localhost:2375")).build();
		final List<Container> containers = docker.listContainers(ListContainersParam.allContainers());
		for (Container container : containers) {
			System.out.println(container.id());
		}
		// docker.startContainer("containerID");
	}

	@Test
	public void run() throws DockerException, InterruptedException {
		final DockerClient docker = DefaultDockerClient.builder().uri(URI.create("http://localhost:2375")).connectionPoolSize(3) .build();
		final ContainerCreation container = docker
				.createContainer(ContainerConfig.builder().image("duckulatask:1").cmd("aa", "8778", "9779").build());
		System.out.println(container.id());
		docker.startContainer(container.id());
	}
}
