package net.wicp.tams.duckula.server.connector;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import net.wicp.tams.common.Result;
import net.wicp.tams.common.apiext.MonitorUtil;
import net.wicp.tams.common.beans.MonitorInfo;
import net.wicp.tams.common.connector.beans.CusDynaBean;
import net.wicp.tams.common.connector.executor.IBusiApp;

@Service(value = "MonitorShow")
@Slf4j
public class MonitorShow implements IBusiApp {
	@Override
	public CusDynaBean exe(CusDynaBean inputBean, CusDynaBean outBeanOri) {
		String needCpu = inputBean.getStrValueByName("needCpu");
		MonitorUtil service = new MonitorUtil();
		try {
			MonitorInfo monitorInfoBean = service.getMonitorInfo(Boolean.valueOf(needCpu));
			outBeanOri.set("monitorInfo", monitorInfoBean);
			outBeanOri.setResult(Result.getSuc());
		} catch (Exception e) {
			log.error("获得服务器状态信息错误", e);
			outBeanOri.setResult(Result.getError(e.getMessage()));
		}
		return outBeanOri;
	}

}
