package net.wicp.tams.duckula.server.service;

import java.io.File;
import java.util.Properties;

import net.wicp.tams.common.apiext.IOUtil;

public abstract class ServerAssit {
	public static String findContainerId(String taskId) {
		String path = IOUtil.mergeFolderAndFilePath(System.getenv("DUCKULA_HOME"), "/logs/docker/dockerinfo.log");
		Properties prop = IOUtil.fileToProperties(new File(path));
		String containerId = String.valueOf(prop.get(taskId));
		return containerId;
	}
}
