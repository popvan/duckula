package net.wicp.tams.duckula.server.constant;

public enum Command {
	TaskStart("启动任务", 1, "TaskStart"),
	
	TaskStop("停止任务", 2, "TaskStop"),

	MonitorShow("显示服务器信息", 3, "MonitorShow");

	private final String desc;
	private final int value;
	private final String key;

	private Command(String desc, int value, String key) {
		this.desc = desc;
		this.value = value;
		this.key = key;
	}

	public String getDesc() {
		return desc;
	}

	public int getValue() {
		return value;
	}

	public String getKey() {
		return key;
	}

	public static Command getByName(String name) {
		for (Command command : Command.values()) {
			if (command.name().equalsIgnoreCase(name)) {
				return command;
			}
		}
		return null;
	}
}
