package net.wicp.tams.duckula.task.jmx;

import org.apache.curator.framework.recipes.locks.InterProcessMutex;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.wicp.tams.common.apiext.LoggerUtil;
import net.wicp.tams.common.constant.JvmStatus;
import net.wicp.tams.duckula.task.Main;

@Data
@Slf4j
public class BinlogControl implements BinlogControlMBean {
	private InterProcessMutex lock;

	@Override
	public void stop() {
		log.info("通过MBean服务停止服务");
		try {
			lock.release();
		} catch (Exception e) {
			log.error("解锁失败", e);
		}
		LoggerUtil.exit(JvmStatus.s15);
	}

	@Override
	public void putSync(boolean isSync) {
		Main.context.setSync(isSync);
	}

	@Override
	public boolean findSync() {
		return Main.context.isSync();
	}

	public void setLock(InterProcessMutex lock) {
		this.lock = lock;
	}

}
