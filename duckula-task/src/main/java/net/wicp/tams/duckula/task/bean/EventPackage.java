package net.wicp.tams.duckula.task.bean;


import lombok.Data;
import net.wicp.tams.duckula.common.beans.Pos;
import net.wicp.tams.duckula.plugin.bean.DuckulaPackage;
import net.wicp.tams.duckula.plugin.bean.Rule;


@Data
public class EventPackage extends DuckulaPackage{
	private boolean isXid;//事务是否结束，xid事件发生即结束
	private Rule rule;//规则
	private int[] partitions;
	private Pos pos;	
}
