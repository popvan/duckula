package net.wicp.tams.duckula.task.jmx;

public interface BinlogControlMBean {
	/***
	 * 停止进程
	 */
	public void stop();

	/***
	 * 设置是否同步
	 * 
	 * @param isSync
	 *            true:是 false:否
	 */
	public void putSync(boolean isSync);

	/**
	 * 查看是否同步
	 * 
	 * @return
	 */
	public boolean findSync();
}
